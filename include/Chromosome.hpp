#pragma once

#include <vector>
#include <ostream>

namespace YAGA{

    class Chromosome{
        std::vector<double>  _data;
        std::pair<double,double>    _range;
        unsigned int    _size;

    public:
        Chromosome():
            _data(),
            _range(),
            _size(0){

        }

        Chromosome(std::vector<double> data):
            _data(data),
            _range(),
            _size(data.size())
        {}

        Chromosome(std::pair<double,double> range, unsigned int size = 0):
            _data(),
            _range(range),
            _size(size){
            _data.reserve(size);

        }

        Chromosome(const Chromosome &orig){
            _data = std::vector<double>( *orig.getDataConst());
            _range = std::pair<double,double>(orig.getRange());
            _size = _data.size();
        }

        const std::pair<double,double>& getRange()const{
            return _range;
        }

        void setRange(const std::pair<double,double> & range){
            _range = range;
        }

        const std::vector<double>* getDataConst()const {
            return &_data;
        }

        std::vector<double>* getData(){
            return &_data;
        }

        void setDatas(const std::vector<double> &datas){
            _data = datas;
        }

        unsigned int getSize() const{
            return _data.size();
        }

        double& at(unsigned int index){
          return _data.at(index);
        }

    };
}

std::ostream& operator<<(std::ostream &f, const YAGA::Chromosome & ch){
    f << "Size : " << ch.getSize() << " Range : [" << ch.getRange().first << " ; " << ch.getRange().second << " ]";
    f << " Datas : #[ ";
    for(auto i : *ch.getDataConst())
        f << i << " ; ";
    f << "] ";
    return f;
}