#pragma once

#include "Chromosome.hpp"
#include <vector>
#include <algorithm>
#include <stdexcept>

using namespace std;
namespace YAGA {

    class ChromosomeManager {
        random_device _rd;
        mt19937 _gen;



    public:

        ChromosomeManager():
            _rd(),
            _gen(_rd()){

        }

        Chromosome generateEmpty() {
            return Chromosome();
        }

        Chromosome duplicate(const Chromosome &ch) {
            return Chromosome(ch);
        }

        Chromosome generateWithRange(double a, double b, unsigned int size){
            return generateWithRange(make_pair(a, b), size);
        }
        Chromosome generateWithRange(pair<double, double> range, unsigned int size){
            Chromosome ch(range);
            vector<double>* v = ch.getData();
            uniform_real_distribution<double> distribution(range.first, range.second);

            for(unsigned int i = 0; i < size; ++i)
                v->push_back(distribution(_gen));

            return ch;

        }

        Chromosome generateWithRange(const Chromosome & ch){
            return generateWithRange(ch.getRange(), ch.getSize());
        }

        Chromosome cross(const Chromosome &ch1, const Chromosome &ch2) {
            vector<unsigned int> indCross;
            indCross.push_back( ch1.getSize() / 2);
            return cross(ch1, ch2, indCross);
        }


        Chromosome mutate(const Chromosome & ch1, double mutRate){
            Chromosome ch(ch1);
            uniform_real_distribution<double> distribution(ch1.getRange().first, ch1.getRange().second);
            uniform_real_distribution<double> mutation(0.0, 1.0);

            vector<double>* v = ch.getData();

            for(unsigned int i = 0; i < v->size(); ++i)
                if(mutation(_gen) <= mutRate)
                    v->at(i) = distribution(_gen);

            return ch;

        }

        Chromosome cross(const Chromosome &ch1, const Chromosome &ch2, std::vector<unsigned int> indCross) {
            Chromosome ch(ch1.getRange(), ch1.getSize());

            if( ch1.getSize() != ch2.getSize())
                throw invalid_argument("The data vectors of the two chromosomes do not have the same size");

            vector< const vector<double>* > vDatas;

            vDatas.push_back(ch1.getDataConst());
            vDatas.push_back(ch2.getDataConst());
            unsigned int size = vDatas.front()->size(), index = 0, i = 0;
            vector<double> * vDest =  ch.getData();
            std::sort(indCross.begin(), indCross.end());
            std::vector<unsigned int>::iterator itIndNext = indCross.begin();
            std::vector<double>::const_iterator it = vDatas.front()->begin(), itNext;

            while ((itIndNext != indCross.end()) && (*itIndNext < size)) {

                while(i < *itIndNext){
                    vDest->push_back(*it);
                    ++it;
                    ++i;
                }

                index = (index + 1) % 2;
                it = vDatas.at(index)->begin() + i;
                ++itIndNext;
            }

            while(it != vDatas.at(index)->end()){
                vDest->push_back(*it);
                ++it;
            }
            return ch;
        }

    };
}