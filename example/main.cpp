#include <iostream>
#include <Chromosome.hpp>
#include <ChromosomeManager.hpp>

using namespace std;
using namespace YAGA;

int main() {
    ChromosomeManager man;
    Chromosome ch1 = man.generateWithRange(0.0, 100.0, 10);
    Chromosome ch2 = man.generateWithRange(0.0, 100.0, 10);

    Chromosome ch3 = man.cross(ch1, ch2, {5 , 2 , 8});

    Chromosome ch4 = man.mutate(ch1, 0.5);
    cout<< ch1 << endl;
    cout<< ch4 << endl;
    //cout<< ch3 << endl;
    return 0;
}